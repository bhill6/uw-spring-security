/**
 * 
 */
package edu.wisc.uwss.configuration.preauth;

import javax.servlet.Filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import edu.wisc.uwss.configuration.HttpSecurityAmender;
import edu.wisc.uwss.configuration.combined.CombinedAuthentication;
import edu.wisc.uwss.preauth.PreAuthLogoutSuccessHandler;
import edu.wisc.uwss.preauth.UWUserDetailsAuthenticationFilter;

/**
 * {@link WebSecurityConfigurerAdapter} for the "preauth" {@link Profile}.
 * The intended use by others is to extend this class, Override the {@link #configure(HttpSecurity)} method (invoking super - this implementation), 
 * and adding whatever additional functionality on {@link HttpSecurity} is required. Most often, you will only need to detail your URI
 * security (using {@link HttpSecurity#authorizeRequests()}).
 * 
 * @author Nicholas Blair
 */
@Configuration
@Profile({ "preauth", "edu.wisc.uwss.preauth" })
@Order(CombinedAuthentication.PREAUTH_WEB_SECURITY_CONFIGURATION_ORDER)
public class PreAuthenticationWebSecurityConfiguration  {
  
  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired
  private AuthenticationManager authenticationManager;

  @Bean
  public HttpSecurityAmender preAuthenticationHttpSecurityAmender() {
    return new PreAuthenticationHttpSecurityAmender(authenticationFilter());
  }
  /**
   * 
   * @return an instance of {@link UWUserDetailsAuthenticationFilter}
   */
  @Bean
  public UWUserDetailsAuthenticationFilter authenticationFilter() {
      UWUserDetailsAuthenticationFilter filter = new UWUserDetailsAuthenticationFilter();
      filter.setAuthenticationManager(authenticationManager);
      return filter;
  }

  /**
   * {@inheritDoc}
   * 
   * This method is required to properly inject the {@link AuthenticationProvider} provided by
   * PreAuthenticationSecurityConfiguration into the AuthenticationManager.
   * 
   * @see AuthenticationManagerBuilder#userDetailsService(UserDetailsService)
   */
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth, AuthenticationProvider authenticationProvider) throws Exception {
    logger.debug("enter WebSecurityConfigurerAdapter#configureGlobal for preauth, injecting {} into AuthenticationManagerBuilder", authenticationProvider);
    auth.authenticationProvider(authenticationProvider);
  }

}
