/**
 * 
 */
package edu.wisc.uwss.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

/**
 * Root {@link Configuration} class that consuming applications should reference in their ApplicationInitizalizers.
 * 
 * ComponentScans all sub-configurations automatically. Each sub-configuration is enabled by turning on
 * Spring Profiles.
 * 
 * @author Nicholas Blair
 */
@Configuration
@ComponentScan(basePackages={
    "edu.wisc.uwss.configuration.local", 
    "edu.wisc.uwss.configuration.preauth" })
@Import(UWSpringSecurityWebConfigurerAdapter.class)
public class UWSpringSecurityConfiguration {

  private static final Logger logger = LoggerFactory.getLogger(UWSpringSecurityConfiguration.class);

  /**
   * 'local-users' and 'preauth' Profiles were replaced by 'edu.wisc.uwss.local-users' and
   * 'edu.wisc.uwss.preauth', respectively.
   *
   * Part of the deprecation path for those legacy profiles is to warn deployers to use their
   * replacements instead.
   *
   * @param environment
   */
  @Autowired
  void warnOnDeprecatedProfiles(Environment environment) {
    if(environment.acceptsProfiles("local-users")) {
      logger.warn("'local-users' profile is deprecated, please use 'edu.wisc.uwss.local-users' instead");
    }
    if(environment.acceptsProfiles("preauth")) {
      logger.warn("'preauth' profile is deprecated, please use 'edu.wisc.uwss.preauth' instead");
    }
  }
}
