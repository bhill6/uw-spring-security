package edu.wisc.uwss.web.shibboleth;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import edu.wisc.uwss.UWUserDetails;

/**
 * Object matching the JSON model from /Shibboleth.sso/Session.json.
 *
 * @author Nicholas Blair
 */
public class SimulatedResponse {

  // computed from session length
  private Integer expiration = 480;
  private String client_address;
  private final String protocol = "urn:oasis:names:tc:SAML:2.0:protocol";
  private final String identity_provider = "https://logintest.wisc.edu/idp/shibboleth";
  private final String authn_instant = LocalDateTime.now(ZoneOffset.UTC).toString();
  private final String authncontext_class = "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport";

  private List<Attribute> attributes = new ArrayList<>();

  public SimulatedResponse(UWUserDetails userDetails, String client_address) {
    this.client_address = client_address;

    List<Attribute> attributes = new ArrayList<>();
    if(StringUtils.isNotBlank(userDetails.getEppn())) {
      attributes.add(new Attribute("eppn", userDetails.getEppn()));
    }
    attributes.add(new Attribute("persistent-id", "https://logintest.wisc.edu/idp/shibboleth!https://fake.wisc.edu/shibboleth!thisis/fake/PE="));
    attributes.add(new Attribute("uid", userDetails.getUsername()));
    attributes.add(new Attribute("pubcookie-user", userDetails.getUsername()));
    if(StringUtils.isNotBlank(userDetails.getPvi())) {
      attributes.add(new Attribute("wiscEduPVI", userDetails.getPvi()));
    }

    this.attributes = Collections.unmodifiableList(attributes);
  }

  public Integer getExpiration() {
    return expiration;
  }

  public String getClient_address() {
    return client_address;
  }

  public String getProtocol() {
    return protocol;
  }

  public String getIdentity_provider() {
    return identity_provider;
  }

  public String getAuthn_instant() {
    return authn_instant;
  }

  public String getAuthncontext_class() {
    return authncontext_class;
  }

  public List<Attribute> getAttributes() {
    return attributes;
  }
}
