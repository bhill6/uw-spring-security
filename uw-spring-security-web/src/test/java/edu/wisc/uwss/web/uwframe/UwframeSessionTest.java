package edu.wisc.uwss.web.uwframe;

import org.junit.Test;
import org.springframework.core.env.Environment;
import org.springframework.mock.env.MockEnvironment;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link UwframeSession}.
 *
 * @author Nicholas Blair
 */
public class UwframeSessionTest {

  @Test
  public void empty() {
    UwframeSession session = new UwframeSession();
    assertEquals("anonymousUser", session.getUsername());
    assertEquals("Guest", session.getDisplayName());
    assertEquals("Guest", session.getFirstName());
    assertEquals("", session.getLastName());
    assertEquals("", session.getVersion());
  }

  @Test
  public void with_empty_Environment() {
    Environment environment = new MockEnvironment();
    UwframeSession session = new UwframeSession(environment, "jim", "James Badger", "James", "Badger");
    assertEquals("jim", session.getUsername());
    assertEquals("James Badger", session.getDisplayName());
    assertEquals("James", session.getFirstName());
    assertEquals("Badger", session.getLastName());
    assertEquals("", session.getVersion());
  }
  @Test
  public void with_complete_Environment() {
    MockEnvironment environment = new MockEnvironment();
    environment.setProperty(UwframeSession.GIT_COMMIT_ID_ABBREV, "abcde2");
    UwframeSession session = new UwframeSession(environment, "jim", "James Badger", "James", "Badger");
    assertEquals("jim", session.getUsername());
    assertEquals("James Badger", session.getDisplayName());
    assertEquals("James", session.getFirstName());
    assertEquals("Badger", session.getLastName());
    assertEquals("Revision abcde2", session.getVersion());
  }
}
