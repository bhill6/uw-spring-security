/**
 * 
 */
package edu.wisc.uwss.sample;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import edu.wisc.uwss.configuration.UWSpringSecurityConfiguration;
import edu.wisc.uwss.sample.configuration.ApplicationConfiguration;
import edu.wisc.uwss.sample.configuration.DemonstrationOnlyPreAuthenticationConfiguration;
import edu.wisc.uwss.sample.configuration.SampleWebSecurityConfiguration;
import edu.wisc.uwss.sample.configuration.WebConfiguration;

/**
 * Servlet 3 application initializer.
 * 
 * @author Nicholas Blair
 */
public class SampleApplicationInitializer extends
    AbstractAnnotationConfigDispatcherServletInitializer {

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getRootConfigClasses()
   */
  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class<?>[] { ApplicationConfiguration.class, UWSpringSecurityConfiguration.class, SampleWebSecurityConfiguration.class, DemonstrationOnlyPreAuthenticationConfiguration.class };
  }

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer#getServletConfigClasses()
   */
  @Override
  protected Class<?>[] getServletConfigClasses() {
      return new Class<?>[] { WebConfiguration.class };
  }

  /* (non-Javadoc)
   * @see org.springframework.web.servlet.support.AbstractDispatcherServletInitializer#getServletMappings()
   */
  @Override
  protected String[] getServletMappings() {
      return new String[] { "/" };
  }

}
