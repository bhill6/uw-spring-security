/**
 *
 */
package edu.wisc.uwss.local;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import edu.wisc.uds.person.Appointment;
import edu.wisc.uds.person.Appointments;
import edu.wisc.uds.person.Demographic;
import edu.wisc.uds.person.Employee;
import edu.wisc.uds.person.Identifier;
import edu.wisc.uds.person.Identifiers;
import edu.wisc.uds.person.Name;
import edu.wisc.uds.person.Person;
import edu.wisc.uds.person.UDDS;
import edu.wisc.uds.UdsPersonService;
import edu.wisc.uwss.UWUserDetails;

/**
 * Implementation of {@link UdsPersonService} that is intended for use
 * with the local development "local-users" {@link Profile}.
 *
 * @author Nicholas Blair
 */
@Profile({ "local-users", "edu.wisc.uwss.local-users" })
public class LocalUsersUdsPersonServiceImpl implements UdsPersonService {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  @Autowired
  private UserDetailsService userDetailsService;
  /* (non-Javadoc)
   * @see edu.wisc.uds.UdsPersonService#getPerson(edu.wisc.uds.person.Identifiers)
   */
  @Override
  public Person getPerson(Identifiers identifiers) {
    for(Identifier identifier: identifiers.getIdentifiers()) {
      try {
        UWUserDetails userDetails = (UWUserDetails) userDetailsService.loadUserByUsername(identifier.getValue());

        // method won't return null by contract, it'll throw UsernameNotFoundException instead
        Person result = new Person();
        Demographic demographic = new Demographic();
        demographic.setEmail(userDetails.getEmailAddress());
        Name name = new Name();
        name.setFirst(userDetails.getFirstName());
        name.setLast(userDetails.getLastName());
        name.setFull(userDetails.getFullName());
        demographic.setName(name);

        result.setDemographic(demographic);
        Employee employee = new Employee();
        employee.setAppointments(new Appointments());
        for(String udds : userDetails.getUddsMembership()) {
          Appointment appointment = new Appointment();
          UDDS u = new UDDS();
          u.setCode(udds);
          appointment.setUDDS(u);
          employee.getAppointments().getAppointments().add(appointment);
        }

        result.setEmployee(employee);
        return result;
      } catch (UsernameNotFoundException e) {
        logger.debug("no userDetails found for " + identifier, e);
      }
    }
    return null;
  }

  /* (non-Javadoc)
   * @see edu.wisc.uds.UdsPersonService#getPeople(java.util.List)
   */
  @Override
  public List<Person> getPeople(List<Identifiers> identifiers) {
    throw new UnsupportedOperationException("source to implement getPeople(List) not provided via Spring's UserDetailsService");
  }

}
