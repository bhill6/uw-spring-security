package edu.wisc.uwss.uds;

import java.util.ArrayList;
import java.util.List;

import edu.wisc.uds.person.Identifier;
import edu.wisc.uds.person.Identifiers;

/**
 * Builder pattern for constructing {@link IdentifiersBuilder}.
 *
 * This class is not thread safe and is intended for use as a prototype.
 *
 * Example usage:
 * <pre>
    new IdentifiersBuilder()
      .withNetid("bbadger")
      .withPvi("UW000A000)
      .toIdentifiers();
 * </pre>
 *
 * Will return:
 *
 * <pre>
 <uds:Identifiers>
 <uds:Identifier>
 <uds:Source>UWMSNSUDS</uds:Source>
 <uds:IdName>NETID</uds:IdName>
 <uds:Value>bbadger</uds:Value>
 </uds:Identifier>
 <uds:Identifier>
 <uds:Source>UWMSNSUDS</uds:Source>
 <uds:IdName>PVI</uds:IdName>
 <uds:Value>UW111Z000</uds:Value>
 </uds:Identifier>
 </uds:Identifiers>
 * </pre>
 *
 * @author Nicholas Blair
 */
public class IdentifiersBuilder {
  private final Identifiers identifiers = new Identifiers();
  public static final String EMPLID = "EMPLID";
  public static final String UWHRS = "UWHRS";
  public static final String UWMSNSUDS = "UWMSNSUDS";
  public static final String NETID = "NETID";
  public static final String PHOTOID = "PHOTOID";
  public static final String PVI = "PVI";
  /**
   *
   * @return the accumulated {@link Identifiers}
   */
  public Identifiers toIdentifiers() {
    return identifiers;
  }
  /**
   * Add an {@link Identifier} for a NetID.
   *
   * @param value the NetID value
   * @return this
   */
  public IdentifiersBuilder withNetid(String value) {
    return with(NETID, UWMSNSUDS, value);
  }
  /**
   * Add an {@link Identifier} for a PVI.
   *
   * @param value the PVI value
   * @return this
   */
  public IdentifiersBuilder withPvi(String value) {
    return with(PVI, UWMSNSUDS, value);
  }
  /**
   * Add an {@link Identifier} for an HRS emplid.
   * Note: the source on this Identifier is {@link #UWHRS}.
   *
   * @param value the emplid value
   * @return this
   */
  public IdentifiersBuilder withHrsEmplid(String value) {
    return with(EMPLID, UWHRS, value);
  }
  /**
   * Add an {@link Identifier} for a Photoid.
   *
   * @param value the photoid value
   * @return this
   */
  public IdentifiersBuilder withPhotoid(String value) {
    return with(PHOTOID, UWMSNSUDS, value);
  }

  /**
   * Add an {@link Identifier}.
   *
   * @param idName {@link Identifier#getIdName()}
   * @param source {@link Identifier#getSource()}
   * @param value {@link Identifier#getValue()}
   * @return this
   */
  public IdentifiersBuilder with(String idName, String source, String value) {
    identifiers.getIdentifiers().add(makeIdentifier(idName, source, value));
    return this;
  }
  /**
   * Utility method to construct a new {@link Identifier}
   *
   * @param idName {@link Identifier#getIdName()}
   * @param source {@link Identifier#getSource()}
   * @param value {@link Identifier#getValue()}
   * @return a new {@link Identifier}
   */
  protected Identifier makeIdentifier(String idName, String source, String value) {
    Identifier identifier = new Identifier();
    identifier.setIdName(idName);
    identifier.setSource(source);
    identifier.setValue(value);
    return identifier;
  }

}
