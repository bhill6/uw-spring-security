/**
 * 
 */
package edu.wisc.uwss;


/**
 * Marker interface indicating this object has a modifiable "source" field.
 * 
 * @author Nicholas Blair
 */
public interface HasModifiableSource {

  /**
   * Set the value for the source field.
   *  
   * @param source the value to set
   */
  void setSource(String source);
  
}
